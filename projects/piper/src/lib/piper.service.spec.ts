import { TestBed } from '@angular/core/testing';

import { PiperService } from './piper.service';

describe('PiperService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PiperService = TestBed.get(PiperService);
    expect(service).toBeTruthy();
  });
});
