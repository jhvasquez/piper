import { NgModule } from '@angular/core';
import { PiperComponent } from './piper.component';
import { SharedModuleModule } from './shared-module/shared-module.module';

@NgModule({
  declarations: [PiperComponent],
  imports: [
    SharedModuleModule
  ],
  exports: [
    PiperComponent,
    SharedModuleModule
  ]
})
export class PiperModule { }
